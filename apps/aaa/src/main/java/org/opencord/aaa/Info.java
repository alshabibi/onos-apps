package org.opencord.aaa;

/**
 * Misc binding info repository.
 */
public class Info {

    private final short stag;
    private final short ctag;
    private final String nasPortId;

    public Info(short stag, short ctag, String nasPortId) {
        this.stag = stag;
        this.ctag = ctag;
        this.nasPortId = nasPortId;
    }

    public short stag() {
        return stag;
    }

    public short ctag() {
        return ctag;
    }

    public String nasPortId() {
        return nasPortId;
    }
}
